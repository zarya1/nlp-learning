# NLP Applications

NLP applications use LLM based on transformer architecture. The three modern LLM implementations are BERT (best for text classification), GPT (best for text generation), and T5 (both used for text classification and generation). BERT is derived from the transformer encoder stack, while GPT is derived from the decoder stack. In each of the use cases, we had several pros and cons. For BERT, we had faster processing for natural language comprehension tasks like classification or token classification, but we lost the flexibility of being able to create things like prompts in our language model. And we also missed some auto-regressive use cases. T5 is an implementation of the entire transformer, both encoder stack and decoder stack.

### Architecture
- [transformers](https://proceedings.neurips.cc/paper_files/paper/2017/file/3f5ee243547dee91fbd053c1c4a845aa-Paper.pdf)

### NLP and modern LLM
![nlp](./nlp.png)

