# LLM: How to train a faster, more compact, yet highly accurate model?

LLM fine-tuning is a technique in natural language processing (NLP) that involves adapting a pre-trained LLM to a specific task by fine-tuning it on a task-specific dataset.
In this exercise, we will learn how to optimize our base model using Distillation, Quantization, and ORT (ONNX Runtime).

### LLM Optimization techniques
![llm optimization](./BERT-Optimization.png)

