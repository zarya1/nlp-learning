# Question-Answering tasks with BERT

## Long Contexs
For QA, context can be very long and BERT can only handle a limited number of tokens. We can't truncate the context, as it can cut off the an answer. What is the solution?

### Split the context into multiple windows
![contextwindow](./contextwindow.png)

#### Problem
The answer might get cut off if it's on the boundary

### Solution: split and overlap de context windows
![contextwindowoverlap](./contextsplitoverlap.png) 
